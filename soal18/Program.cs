﻿var makan = new List<kue>
{
    new kue()
    {
        Jam=9,
        Kalori=30
    },
    new kue()
    {
        Jam=13,
        Kalori=20
    },
    new kue()
    {
        Jam=15,
        Kalori=50
    },
    new kue()
    {
        Jam=17,
        Kalori=80
    }
};
int mulaiOlahraga=18;
double nMenitOlahraga = 0;
foreach(var konsumsi in makan)
{
    nMenitOlahraga += .1 * konsumsi.Kalori * (mulaiOlahraga - konsumsi.Jam);
}
Console.WriteLine($"Donna berolahraga selama {nMenitOlahraga} menit dengan meminum air sebanyak {Math.Floor(nMenitOlahraga / 30) * 100 + 500}cc");
class kue
{
    public int Jam { get; set; }
    public int Kalori { get; set; }
}