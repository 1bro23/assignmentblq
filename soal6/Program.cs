﻿Console.Write("Masukkan kata : ");
var kata = Console.ReadLine();
bool isPalindrome(string kata)
{
    for (int i = 0; i < kata.Length; i++)
    {
        if (kata[i] != kata[kata.Length - 1 - i]) return false;
    }
    return true;
}
if (isPalindrome(kata)) Console.WriteLine($"{kata} adalah palindrome");
else Console.WriteLine($"{kata} tidak palindrome");