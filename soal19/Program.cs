﻿var sentences = new string[] {
    "Sphinx of black quartz, judge my vow",
    "Brawny gods just flocked up to quiz and vex him",
    "Check back tomorrow; I will see if the book has arrived."
};
bool pangram(string sentence)
{
    sentence = sentence.ToLower();
    for (int i=0;i<26;i++)
    {
        char c = Convert.ToChar(i + 97);
        if (!sentence.Contains(c)) return false;
    }
    return true;
}
foreach(var sentence in sentences)
{
    if (pangram(sentence)) Console.WriteLine($"Kalimat:{sentence} adalah pangram");
    else Console.WriteLine($"Kalimat:{sentence} bukan pangram");
}