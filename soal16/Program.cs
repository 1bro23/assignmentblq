﻿var pesanan = new List<Menu>()
{
    new Menu()
    {
        Nama="Tuna Sandwich",
        Harga=42000,
        Ikan=true,
    },
    new Menu()
    {
        Nama="Spaghetti Carbonara",
        Harga=50000
    },
    new Menu()
    {
        Nama="Tea pitcher",
        Harga=30000
    },
    new Menu()
    {
        Nama="Pizza",
        Harga=70000
    },
    new Menu()
    {
        Nama="Salad",
        Harga=30000
    }
};
var persons = new List<Person>()
{
    new Person()
    {
        Nama="Myself"
    },
    new Person()
    {
        Nama="teman1"
    },
    new Person()
    {
        Nama="teman2"
    },
    new Person()
    {
        Nama="teman3",
        AlergiIkan=true
    }
};
int totalTemanNormal = persons.Count(e => !e.AlergiIkan);
foreach(var item in pesanan)
{
    double hargaKotor = item.Harga * 1.15;
    if (item.Ikan)
    {
        foreach(var person in persons)
        {
            if (!person.AlergiIkan) person.Pengeluaran += hargaKotor / totalTemanNormal;
        }
    }
    else
    {
        foreach(var person in persons)
        {
            person.Pengeluaran += hargaKotor / persons.Count;
        }
    }
}
Console.WriteLine("Pesanan:");
foreach(var item in pesanan)
{
    Console.WriteLine($"{item.Nama}:{item.Harga}{(item.Ikan ? ",Mengandung Ikan" : "")}");
}
Console.WriteLine("Pengeluaran:");
foreach (var person in persons)
{
    Console.WriteLine($"{person.Nama} : {person.Pengeluaran}"); 
}
class Menu
{
    public string Nama { get; set; }
    public int Harga { get; set; }
    public bool Ikan { get; set; } = false;
}
class Person
{
    public string Nama { get; set; }
    public double Pengeluaran { get; set; } = 0;
    public bool AlergiIkan { get; set; } = false;
}