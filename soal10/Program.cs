﻿var namas = new List<string>() { "Susilo Bambang Yudhoyono", "Rani Tiara" };
void alterSentence(string sentence)
{
    var namaParse = sentence.Split(' ');
    for (int i = 0; i < namaParse.Length; i++)
    {
        string kata = namaParse[i];
        string newKata = "";
        for (int j = 0; j < namaParse[i].Length; j++)
        {
            if (j == 0 || j == kata.Length - 1) newKata += kata[j];
            else newKata += '*';
        }
        namaParse[i] = newKata;
    }
    Console.WriteLine(sentence+" : "+string.Join(' ', namaParse));
}
foreach(var nama in namas)
{
    alterSentence(nama);
}
