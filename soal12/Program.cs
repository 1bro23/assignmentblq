﻿var deret = new List<int>() { 1, 2, 1, 3, 4, 7, 1, 1, 5, 6, 1, 8 };
Console.WriteLine("input:" + string.Join(',',deret));
for(int i = 0; i < deret.Count; i++)
{
    int memIndex = i;
    for(int j = i+1; j < deret.Count; j++)
    {
        if (deret[j] < deret[memIndex]) memIndex = j;
    }
    int temp = deret[i];
    deret[i] = deret[memIndex];
    deret[memIndex] = temp;
}
Console.WriteLine("Output:" + string.Join(',', deret));