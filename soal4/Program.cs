﻿bool isPrime(int n)
{
    if (n == 0 || n == 1) return false;
    for (int i = 2; i < n; i++)
    {
        if(n%i==0) return false;
    }
    return true;
}

Console.Write("Masukkan n bilangan prima : ");
int jumlah = int.Parse(Console.ReadLine());
var primes = new List<int>();
int number = 2;
while (primes.Count < jumlah)
{
    if (isPrime(number))
    {
        primes.Add(number);
    }
    number++;
}
Console.WriteLine(string.Join(',',primes));
