﻿//var kaca_mata = new int[] { 500, 600, 700, 800 };
//var baju = new int[] { 200, 400, 350 };
//var sepatu = new int[] { 400, 350, 200, 300 };
//var buku = new int[] { 100, 50, 150 };
var label = new string[] { "kaca_mata", "baju", "sepatu", "buku"};
List<List<int>> list = new List<List<int>>()
{
    new List<int>() { 500,600,700,800},
    new List<int>() { 200, 400, 350},
    new List<int>() { 400, 350, 200, 300},
    new List<int>() { 100, 50, 150},
};
string output="";
int lastPrice;
Console.Write("Masukkan jumlah uang:");
int uang = int.Parse(Console.ReadLine());
for (int i = 0; i < 4; i++)
{
    foreach(var item in list[i])
    {
        //Console.WriteLine($"{i},{item}");
        if (item == uang) output = $"Jumlah Uang yang dipakai:{item}\nJumlah item yang bisa dibeli:1({label[i]} {item})";
    }
}
int lastDif = 999999;
for (int i = 0; i < 4; i++)
{
    for(int j = i+1; j < 4; j++)
    {
        foreach(var item in list[i])
        {
            foreach(var item2 in list[j])
            {
                //Console.WriteLine($"{i},{j},{item + item2}");
                if (item + item2 <= uang && uang - (item + item2)<=lastDif)
                {
                    output = $"Jumlah Uang yang dipakai:{item + item2}\nJumlah item yang bisa dibeli:2({label[i]} {item},{label[j]} {item2})";
                    lastDif = uang - (item + item2);
                }

            }
        }
    }
}
lastDif = 999999;

for (int i = 0; i < 4; i++)
{
    for (int j = i + 1; j < 4; j++)
    {
        for(int k = j + 1; k < 4; k++)
        {
            foreach(var item in list[i])
            {
                foreach(var item2 in list[j])
                {
                    foreach(var item3 in list[k])
                    {
                        //Console.WriteLine($"{i},{j},{k},{item+item2+item3}");
                        if (item + item2 + item3 <= uang && uang - (item + item2 + item3) <= lastDif)
                        {
                            output = $"Jumlah Uang yang dipakai:{item + item2 + item3}\nJumlah item yang bisa dibeli:3({label[i]} {item},{label[j]} {item2},{label[k]} {item3})";
                            lastDif = uang - (item + item2 + item3);
                        }
                    }
                }
            }
        }
    }
}
lastDif = 999999;

foreach (var item in list[0])
{
    foreach(var item2 in list[1])
    {
        foreach(var item3 in list[2])
        {
            foreach(var item4 in list[3])
            {
                //Console.WriteLine($"{1},{2},{3},{4},{item + item2 + item3+item4}");
                if (item + item2 + item3 + item4 <= uang && uang - (item + item2 + item3 + item4) <= lastDif)
                {
                    output = $"Jumlah Uang yang dipakai:{item + item2 + item3 + item4}\nJumlah item yang bisa dibeli:4({label[0]} {item},{label[1]} {item2},{label[2]} {item3},{label[3]} {item4})";
                    lastDif = uang - (item + item2 + item3 + item4);
                }

            }
        }
    }
}
Console.WriteLine(output);