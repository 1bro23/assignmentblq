﻿using System.Globalization;

var pinjam = DateTime.Parse("28 Februari 2016",CultureInfo.GetCultureInfo("id-ID"));
var kembali = DateTime.Parse("7 Maret 2016", CultureInfo.GetCultureInfo("id-ID"));
var dayLong = kembali - pinjam;
var label = new string[] { "A", "B", "C", "D" };
var duration = new int[] { 14, 3, 7, 7 };
Console.WriteLine("Peminjaman Rentang waktu 28 Februari 2016 - 7 Maret 2016");
for(int i = 0; i < label.Length; i++)
{
    Console.Write($"Buku {label[i]} denda : ");
    var diff = dayLong.TotalDays - duration[i];
    if (diff>0)
    {
        Console.WriteLine(diff * 100);
    }
    else
    {
        Console.WriteLine(0);
    }
}

pinjam = DateTime.Parse("29 April 2018", CultureInfo.GetCultureInfo("id-ID"));
kembali = DateTime.Parse("30 Mei 2018", CultureInfo.GetCultureInfo("id-ID"));
dayLong = kembali - pinjam;
Console.WriteLine("Peminjaman Rentang waktu 29 April 2018 - 30 Mei 2018");
for (int i = 0; i < label.Length; i++)
{
    Console.Write($"Buku {label[i]} denda : ");
    var diff = dayLong.TotalDays - duration[i];
    if (diff > 0)
    {
        Console.WriteLine(diff * 100);
    }
    else
    {
        Console.WriteLine(0);
    }
}