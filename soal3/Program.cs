﻿using System.Globalization;

var masuk = new string[] { "27 Januari 2019 05:00:01", "27 Januari 2019 07:03:59", "27 Januari 2019 07:05:00", "27 Januari 2019 11:14:23" };
var keluar = new string[] { "27 Januari 2019 17:45:03", "27 Januari 2019 15:30:06", "28 Januari 2019 00:20:21", "27 Januari 2019 13:20:00" };

for(int i = 0; i < masuk.Length; i++)
{
    Console.Write($"masuk:{masuk[i]} - keluar:{keluar[i]} - tarif:");
    int hour = (int)Math.Ceiling((DateTime.Parse(keluar[i],CultureInfo.GetCultureInfo("id-ID")) - DateTime.Parse(masuk[i], CultureInfo.GetCultureInfo("id-ID"))).TotalHours);
    int tarif;
    if(hour <= 8)
    {
        tarif = hour * 1000;
    }
    else if(hour <= 24)
    {
        tarif = 8000;
    }
    else
    {
        tarif = 15000 + (hour - 24) * 1000;
    }
    Console.WriteLine(tarif);
}