﻿var deret = new List<int>() { 8, 7, 0, 2, 7, 1, 7, 6, 3, 0, 7, 1, 3, 4, 6, 1, 6, 4, 3 };
deret.Sort();
Console.WriteLine(string.Join(',', deret));
int mean = deret.Sum()/deret.Count;

var deretLabel = deret.GroupBy(d => d).ToList();
int maxCount = deretLabel.Max(d => d.Count());
int modus = deretLabel.First(d => d.Count() == maxCount).Key;

int n = deret.Count();
int median;
if (n % 2 == 0)
{
    median = (deret[(int)Math.Floor(n / 2.0)] + deret[(int)Math.Ceiling(n / 2.0)]) / 2;
}
else median = deret[n / 2];
Console.WriteLine($"Mean : {mean}, median : {median}, modus : {modus}");
