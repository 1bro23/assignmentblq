﻿var jam = new List<string>() { "3:00","5:30","2:20" };
void sudut(string jam)
{
    var time = DateTime.Parse(jam);
    double hour = (time.Hour % 12)/12.0*360;
    double minute = time.Minute/60.0*360;
    double sudut = Math.Abs(hour - minute);
    if (sudut > 180) sudut -= 180;
    Console.WriteLine($"Jam {jam} => {sudut}");
}
foreach(var item in jam)
{
    sudut(item);
}
