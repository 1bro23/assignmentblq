﻿string track = "_____O___";
int playerStamina = 0;
string solusi = "";

for (int i = 0; i < track.Length; i++)
{
    int hole = track.IndexOf("O_", i);
    if (hole != i + 1 && hole!=-1 && hole!=0)
    {
        playerStamina++;
        solusi += "W ";
    }
    else
    {
        if(playerStamina < 2)
        {
            solusi = "Failed";
            break;
        }
        playerStamina -= 2;
        i += 3;
        solusi += "J ";
    }
}
Console.WriteLine(solusi);