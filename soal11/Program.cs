﻿var katas = new List<string>() { "Afrika","Jeruk"};
foreach(var kata in katas)
{
    Console.WriteLine("Input:" + kata);
    Console.WriteLine("Output:");
    for(int i=kata.Length-1; i>=0; i--)
    {
        Console.WriteLine($"***{kata[i]}***");
    }
}